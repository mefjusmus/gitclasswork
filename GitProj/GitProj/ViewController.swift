//
//  ViewController.swift
//  GitProj
//
//  Created by Vladislav Suslov on 11.11.21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var textField: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func changeTextButton(_ sender: Any) {
        guard let text = textField.text, !text.isEmpty else { return }
        textLabel.text = text
    }
    
}

